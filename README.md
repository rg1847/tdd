TDD Framework
==================

If you're interested in writing more robust code you should really consider writing tests for your code. TDD or Test Driven Development is a principle where you write the test first and then write your code.  It's a common practice among professional developers and now you can do TDD on the Demandware platform with the "TDD Framework for Demandware".

**Main Screen**
> ![list-of-test-suites.png](https://bitbucket.org/repo/nAdrBM/images/3005906755-list-of-test-suites.png)

**Test Suite Screen**
> ![cipherhelpertestsuite.png](https://bitbucket.org/repo/nAdrBM/images/3391737330-cipherhelpertestsuite.png)

Customizing the TDD Framework to add input parameters
=======================================================
**Test Suite Screen with Params**
![TestSuiteWithParams.PNG](https://bitbucket.org/repo/k4Rx7x/images/3951788371-TestSuiteWithParams.PNG)

Currently it is using the approach where we are passing the parameters in JSON String format
and parsing it at the individual test suite level to pass it further to the service.

It can be further modified so that we just need to add the values to the individual parameters instead of adding the JSON string.